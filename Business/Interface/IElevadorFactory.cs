﻿using Business.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interface
{
    public interface IElevadorFactory
    {
        //interface "parar"
        void Parar(int andar, out int andarCorrente, out EnumElevador.ElevadorEstado Status);
        
    }
}
