﻿using Business.Enum;
using Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ElevadorFactory : IElevadorFactory
    {
        //implemetando a interface Parar
        public void Parar(int andar, out int andarCorrente, out EnumElevador.ElevadorEstado Status)
        {
            Status = Business.Enum.EnumElevador.ElevadorEstado.PARADO;
            andarCorrente = andar;
        }
}
}
