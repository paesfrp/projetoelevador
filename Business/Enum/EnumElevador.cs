﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Enum
{
    public class EnumElevador
    {
        //Enum que define os estados do elevador
        public enum ElevadorEstado
        {
            SUBINDO,
            PARADO,
            DESCENDO
        }

        //Enum que define o status da porta do elevador
        public enum ElevadorPorta
        {
            ABERTA = 1,
            FECHADA = 2
        }

        //tipo elevador
        public enum ElevadorTipo
        {
            SOCIAL = 1,
            SERVICO = 2
        }

        //tipo servico
        public enum ElevadorTipoServico
        {
            LIGADO = 1,
            DESLIGADO = 2
        }
    }
}
