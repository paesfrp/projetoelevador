﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjetoElevador
{
    class Program
    {
        //variavel para sair do programa
        //digite "sair"no console
        private const string SAIR = "sair";

        public static void Main(string[] args)
        {
            try {

                Console.WriteLine("**************** Projeto Elevador ****************");
                Console.WriteLine("******** Esse edificio contém 30 Andares *********");
                Console.WriteLine("\n");

                //classe que define elevador
                ElevadorFactory elevador = new ElevadorFactory(30);

                string valorDigitado = string.Empty;
                string escolhaElevador = string.Empty;
                int andar = 0;
                int tipoElevador;
                string escolhaPessoas = string.Empty;

                //variavel de controle de troca de elevador
                int trocaElevador = 0;

                //enquanto o valor digitado for diferente de "sair", os sistema continua em execução.
                while (escolhaElevador != SAIR)
                {
                    //escolha do elevador
                    Console.WriteLine("| Qual elevador deseja utilizar? ");
                    Console.WriteLine("  || 1 - social");
                    Console.WriteLine("  || 2 - serviço");
                    Console.WriteLine("  || Para finalizar o sistema digite\"sair\"");

                    escolhaElevador = Console.ReadLine();
                    //verifica se a escolha é um inteiro
                    if (Int32.TryParse(escolhaElevador, out tipoElevador))
                    {
                        //caso seja do tipo social ou servico prossegue
                        if (tipoElevador == 1 || tipoElevador == 2)
                        {
                            while (escolhaPessoas != SAIR)
                            {
                                //validando o peso das pessoas
                                if (tipoElevador == 1)
                                {
                                    //zerando a variavel de andarCorrente caso troque de elevador
                                    if (tipoElevador != trocaElevador)
                                        elevador.andarCorrente = 0;

                                    trocaElevador = tipoElevador;

                                    Console.WriteLine("     || Elevador Social -> a capacidade máxima do elevador social é de 600kg");
                                    Console.WriteLine("        || Digite quantas pessoas entrarão no elevador");
                                    Console.WriteLine("        || ou caso deseja voltar a seleção de elevadores, digite \"sair\"...\"");
                                    
                                    string qtPessoas = Console.ReadLine();
                                    int qt;
                                    if(qtPessoas == SAIR)
                                    {
                                        break;
                                    }
                                    if (Int32.TryParse(qtPessoas, out qt))
                                    {
                                        bool erro = false;
                                        int peso = 0;
                                        var pesoTotal = 0;
                                        //intera e calcula o peso das pessoas 
                                        for (int i = 0; i < qt; i++)
                                        {
                                            Console.WriteLine("            Pessoa: {0}", i + 1);
                                            Console.WriteLine("            Peso: ");
                                            string pesoPessoa = Console.ReadLine();

                                            if (Int32.TryParse(pesoPessoa, out peso))
                                            {
                                                pesoTotal += peso;
                                            }
                                            else
                                            {
                                                Console.WriteLine("        ****** O peso deve ser um numero! ******");
                                                erro = true;
                                                break;
                                            }
                                        }
                                        if(erro)
                                        {
                                            continue;
                                        }
                                        if (pesoTotal > 600)
                                        {
                                            Console.WriteLine("        ****** Quantidade máxima de peso não permitida! ******");
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("        ****** Quantidade de pessoas deve ser um numero! ******");
                                        continue;
                                    }
                                }
                                //validando o peso da carga
                                else
                                {
                                    //zerando a variavel de andarCorrente caso troque de elevador
                                    if (tipoElevador != trocaElevador)
                                        elevador.andarCorrente = 0;

                                    trocaElevador = tipoElevador;

                                    Console.WriteLine("     || Elevador de Serviço -> a capacidade máxima do elevador de serviço é de 1000kg");
                                    Console.WriteLine("        || Digite quantos itens serão transportados");
                                    Console.WriteLine("        || ou caso deseja voltar a seleção de elevadores, digite \"sair\"...\"");

                                    string qtItem = Console.ReadLine();
                                    int qt;
                                    if (qtItem == SAIR)
                                    {
                                        break;
                                    }
                                    if (Int32.TryParse(qtItem, out qt))
                                    {
                                        bool erro = false;
                                        int peso = 0;
                                        var pesoTotal = 0;
                                        //intera e calcula o peso das pessoas 
                                        for (int i = 0; i < qt; i++)
                                        {
                                            Console.WriteLine("            Item: {0}", i + 1);
                                            Console.WriteLine("            Peso: ");
                                            string pesoPessoa = Console.ReadLine();

                                            if (Int32.TryParse(pesoPessoa, out peso))
                                            {
                                                pesoTotal += peso;
                                            }
                                            else
                                            {
                                                Console.WriteLine("        ****** O peso deve ser um numero! ******");
                                                erro = true;
                                                break;
                                            }
                                        }
                                        if (erro)
                                        {
                                            continue;
                                        }
                                        if (pesoTotal > 1000)
                                        {
                                            Console.WriteLine("        ****** Quantidade máxima de peso não permitida! ******");
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("        ****** Quantidade de item deve ser um numero! ******");
                                        continue;
                                    }
                                }
                                elevador.Elevador(valorDigitado, andar, elevador, tipoElevador, SAIR);
                            }
                        }
                        else
                        {
                            Console.WriteLine("     ****** A escolha do elevador deve ser 1 ou 2! ******");
                        }
                    }
                    // se for digitado "sair" o programa é finalizado
                    else if (escolhaElevador == SAIR)
                    {
                        Console.WriteLine("*** Até Logo! ***");
                    }
                    else
                    {
                        Console.WriteLine("   ****** A escolha do elevador deve ser 1 ou 2! ******");
                    }
                }
            }
            catch(NullReferenceException nre)
            {

            }
            catch(ArgumentException ae)
            {

            }
            catch(Exception ex)
            {

            }
        }

    }

}
