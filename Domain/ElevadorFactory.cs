﻿using Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Domain
{
    public class ElevadorFactory
    {
        //variaveis de controle
        private bool[] andarAlvo;
        public int andarCorrente = 0;
        private int andarMaximo;
        private List<int> andaresVisitados = null;
        public Business.Enum.EnumElevador.ElevadorEstado Status = Business.Enum.EnumElevador.ElevadorEstado.PARADO;
        public Business.Enum.EnumElevador.ElevadorPorta StatusPorta = Business.Enum.EnumElevador.ElevadorPorta.ABERTA;
        public Business.Enum.EnumElevador.ElevadorTipoServico StatusElevadorServico = Business.Enum.EnumElevador.ElevadorTipoServico.LIGADO;

        //implementando Objeto elevadorFactory
        private Business.ElevadorFactory _elevadorFactory;

        //construtor que define a quantidade de andares da classe elevador
        public ElevadorFactory(int numerosDeAndares)
        {
            andarAlvo = new bool[numerosDeAndares + 1];
            andarMaximo = numerosDeAndares;
            andaresVisitados = new List<int>();

            // instanciando o Elevador Factory
            _elevadorFactory = new Business.ElevadorFactory();
        }
        // construtor sem parametros
        public ElevadorFactory()
        {
        }

        // metodo responsavel pela exexucao do elevador
        public void Elevador(string valorDigitado, int andar, ElevadorFactory elevador, int tipoElevador, string SAIR)
        {
            Console.WriteLine("           || Digite o andar que deseja ir:");

            //sistema fica no aguardo de algum valor de entrada
            valorDigitado = Console.ReadLine();
            
            //caso seja um numero valido, o sistema vai ate o andar escolhido
            if (Int32.TryParse(valorDigitado, out andar))
            {
                // lista de andares visitados, servirá para nao exibir o numero de andares ja visitados pelo sistema
                andaresVisitados.Add(andar);

                //evoca o metodo para executar a viagem
                elevador.AndarEscolhido(andar, tipoElevador);
            }
            else if(valorDigitado == SAIR)
            {

            }
            // caso o valor nao encontre nenhum valor válido, mostra a mensagem de erro
            else
            {
                Console.WriteLine("           *** Esse andar não existe, escolha um valor ate 30 ou digite ***");
            }
        }

        //metodo que indica que o elevador esta descendo
        private void Descer(int andar, int tipoElevador)
        {
            Console.WriteLine("              || porta {0}", Business.Enum.EnumElevador.ElevadorPorta.FECHADA);
            Console.WriteLine("              || Status {0}", Business.Enum.EnumElevador.ElevadorEstado.DESCENDO);
            Console.WriteLine("              || O elevador esta descendo para andar {0}", andar);

            //gerando a rota de tragetoria do elevador
            var rota = Array.IndexOf(andarAlvo, true);
            string rotaASeguir = string.Empty;
            for (int i = andarCorrente; i >= rota; i--)
            {
                //valida se os andares ja foram visitados, caso sim nao será exibido nas rotas
                if (!andaresVisitados.Contains(i))
                    rotaASeguir += "-> " + i.ToString();
            }
            Console.WriteLine("                 || Rota que o elevador irá seguir: {0}", rotaASeguir);

            //interacao com o processo de descida do elevador
            for (int i = andarCorrente; i >= 0; i--)
            {
                if (andarAlvo[i])
                {
                    Console.WriteLine("                      {0}º andar", i);
                    _elevadorFactory.Parar(andar, out andarCorrente, out Status);
                    andarAlvo[andar] = false;

                    //adiciona o andar escolhido na lista para nao mostrar na rota novamente
                    andaresVisitados.Add(i);

                    break;
                }
                else
                {
                    Thread.Sleep(1000);
                    Console.WriteLine("                      {0}º andar", i);
                    continue;
                }
            }

            Console.WriteLine("              || O Elevador chegou ao andar {0}, esperando a proxima ação!");
            Console.WriteLine("              || Status {0}", Business.Enum.EnumElevador.ElevadorEstado.PARADO);
            Console.WriteLine("              || Porta {0}", Business.Enum.EnumElevador.ElevadorPorta.ABERTA);
        }

        //metod que indica que o elevador esta subindo
        private void Subir(int andar, int tipoElevador)
        {
            

            //se o tipo de elevador for social, o sistema permite a parada e acao posterior ao chegar no andar escolhido
            if (tipoElevador == 1)
            {
                Console.WriteLine("              || porta {0}", Business.Enum.EnumElevador.ElevadorPorta.FECHADA);
                Console.WriteLine("              || Status {0}", Business.Enum.EnumElevador.ElevadorEstado.SUBINDO);
                Console.WriteLine("              || O elevador esta subindo para andar {0}", andar);

                //gerando a rota de tragetoria do elevador
                var rota = Array.IndexOf(andarAlvo, true);
                string rotaASeguir = string.Empty;
                for (int i = andarCorrente; i <= rota; i++)
                {
                    //valida se os andares ja foram visitados, caso sim nao será exibido nas rotas
                    if (!andaresVisitados.Contains(i))
                        rotaASeguir += "-> " + i.ToString();
                }
                Console.WriteLine("                 || Rota que o elevador irá seguir: {0}", rotaASeguir);

                //interacao processo de subido do elevador
                for (int i = andarCorrente; i <= andarMaximo; i++)
                {
                    if (andarAlvo[i])
                    {
                        Console.WriteLine("                      {0}º andar", i);

                        _elevadorFactory.Parar(andar, out andarCorrente, out Status);
                        andarAlvo[andar] = false;

                        //adiciona o andar escolhido na lista para nao mostrar na rota novamente
                        andaresVisitados.Add(i);

                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        Console.WriteLine("                      {0}º andar", i);
                        continue;
                    }
                }

                Console.WriteLine("              || O Elevador chegou ao andar {0}, esperando a proxima ação!");
                Console.WriteLine("              || Status {0}", Business.Enum.EnumElevador.ElevadorEstado.PARADO);
                Console.WriteLine("              || Porta {0}", Business.Enum.EnumElevador.ElevadorPorta.ABERTA);
            }
            // caso seja de serviço o elevador ira subir e descer
            else
            {
                //verificando a chave "SERVICO" do elevador caso esteja ligada o elevador ignora chamadas internas

                Console.WriteLine("                 || A chave de serviço do elevador esta: {0}", StatusElevadorServico);
                Console.WriteLine("                 || 1 -> Ligar");
                Console.WriteLine("                 || 2 -> Desligar");
                string statusTipoEle = Console.ReadLine();
                int tipo = 0;
                int statusElevador = 0;

                if (Int32.TryParse(statusTipoEle, out tipo))
                {
                    if (tipo != 1 && tipo != 2)
                    {
                        Console.WriteLine("                 ***** Tipo de chave de serviço do elevador invalido ***");
                    }
                    else if(tipo == 1)
                    {
                        StatusElevadorServico = Business.Enum.EnumElevador.ElevadorTipoServico.LIGADO;
                    }
                    else if (tipo == 2)
                    {
                        StatusElevadorServico = Business.Enum.EnumElevador.ElevadorTipoServico.DESLIGADO;
                    }
                }
                else
                {
                    Console.WriteLine("                 ***** Tipo de chave de serviço do elevador invalido ***");
                }

                if (tipo == 2)
                {
                    Console.WriteLine("                 || Tipo de chave de serviço esta: {0}", StatusElevadorServico);

                    Console.WriteLine("              || porta {0}", Business.Enum.EnumElevador.ElevadorPorta.FECHADA);
                    Console.WriteLine("              || Status {0}", Business.Enum.EnumElevador.ElevadorEstado.SUBINDO);
                    Console.WriteLine("              || O elevador esta subindo para andar {0}", andar);

                    //gerando a rota de tragetoria do elevador
                    var rota = Array.IndexOf(andarAlvo, true);
                    string rotaASeguir = string.Empty;
                    for (int i = andarCorrente; i <= rota; i++)
                    {
                        //valida se os andares ja foram visitados, caso sim nao será exibido nas rotas
                        if (!andaresVisitados.Contains(i))
                            rotaASeguir += "-> " + i.ToString();
                    }
                    for (int i = rota; i >= andarCorrente; i--)
                    {
                        //valida se os andares ja foram visitados, caso sim nao será exibido nas rotas
                        if (!andaresVisitados.Contains(i))
                            rotaASeguir += "-> " + i.ToString();
                    }
                    Console.WriteLine("                 || Rota que o elevador irá seguir: {0}", rotaASeguir);

                    //interacao processo de subido do elevador 
                    //no caso do social ele vai ate o andar escolhido e volta para seu estado inicial
                    for (int i = andarCorrente; i <= andarMaximo; i++)
                    {
                        if (andarAlvo[i])
                        {
                            Console.WriteLine("                      {0}º andar", i);
                            Thread.Sleep(1000);

                            andarAlvo[i] = false;

                            //adiciona o andar escolhido na lista para nao mostrar na rota novamente
                            andaresVisitados.Add(i);

                            Console.WriteLine("              || O Elevador chegou ao andar {0}", andar);
                            Thread.Sleep(1000);
                            Console.WriteLine("              || Status {0}", Business.Enum.EnumElevador.ElevadorEstado.PARADO);
                            Thread.Sleep(1000);
                            Console.WriteLine("              || Porta {0}", Business.Enum.EnumElevador.ElevadorPorta.ABERTA);
                            Thread.Sleep(1000);
                            Console.WriteLine("              || Porta {0}", Business.Enum.EnumElevador.ElevadorPorta.FECHADA);
                            Console.WriteLine("              || O elevador esta descendo para andar inicial");

                            //interacao com o processo de descida do elevador
                            for (int j = i; j >= 0; j--)
                            {
                                if (andarAlvo[j])
                                {
                                    _elevadorFactory.Parar(andar, out andarCorrente, out Status);
                                    andarAlvo[andar] = false;
                                    break;
                                }
                                else
                                {
                                    Thread.Sleep(1000);
                                    Console.WriteLine("                      {0}º andar", j);
                                    continue;
                                }
                            }
                            break;
                        }
                        else
                        {
                            Thread.Sleep(1000);
                            Console.WriteLine("                      {0}º andar", i);
                            continue;
                        }
                    }
                    Console.WriteLine("              || O Elevador voltou para o estado inicial esperando a proxima ação!");
                    Console.WriteLine("              || Status {0}", Business.Enum.EnumElevador.ElevadorEstado.PARADO);
                    Console.WriteLine("              || Porta {0}", Business.Enum.EnumElevador.ElevadorPorta.ABERTA);
                }

                else
                {
                    Console.WriteLine("              **** O Elevador esta com a chave ligado, e não aceita chamadas externas");
                }
            }
        }

        //metodo que define o andar escolhido
        public void AndarEscolhido(int andar, int tipoElevador)
        {
            if (andar > andarMaximo)
            {
                Console.WriteLine("        **** O andar que escolheu é maior que a quantidade de 30 andares ****", andarMaximo);
                return;
            }
            // seta o andar alvo que foi escolhido
            andarAlvo[andar] = true;

            //dependendo do estado atual do elevador e andar escolhido, a ação sera realizada
            switch (Status)
            {

                case Business.Enum.EnumElevador.ElevadorEstado.DESCENDO:
                    if ((int)Business.Enum.EnumElevador.ElevadorPorta.FECHADA == 2)
                        Descer(andar, tipoElevador);
                    break;

                case Business.Enum.EnumElevador.ElevadorEstado.PARADO:
                    if (andarCorrente < andar)
                    {
                        if ((int)Business.Enum.EnumElevador.ElevadorPorta.FECHADA == 2)
                            Subir(andar, tipoElevador);
                    }
                    else if (andarCorrente == andar)
                    {
                        Console.WriteLine("        **** O valor digitado é o mesmo que se encontra o elevador... ****");
                    }
                    else
                    {
                        if ((int)Business.Enum.EnumElevador.ElevadorPorta.FECHADA == 2)
                            Descer(andar, tipoElevador);
                    }
                    break;

                case Business.Enum.EnumElevador.ElevadorEstado.SUBINDO:

                    if ((int)Business.Enum.EnumElevador.ElevadorPorta.FECHADA == 2)
                        Subir(andar, tipoElevador);
                    break;

                default:
                    break;
            }


        }


    }
}
